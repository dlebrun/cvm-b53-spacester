package com.example.spacester.scene;

import java.util.List;

import org.andengine.input.touch.TouchEvent;

import android.util.Log;

import com.example.spacester.MainActivity;
import com.example.spacester.R;
import com.example.spacester.entity.Player;
import com.example.spacester.model.ModelPlayer;
import com.example.spacester.sprite.ShipSprite;

import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class GameOverScene extends CVMAbstractScene
{	
	public GameOverScene() 
	{
		super("background/creditscreen/creditscene.png", 5);
	}
	
	@Override
	public void dialogCanceled()
	{
		
	}
	
	@Override
	public void dialogClosing(String playerName)
	{
		ShipSprite.setPlayerName(playerName);
		MainActivity.databasePlayer(playerName, ShipSprite.getPlayerScores(), getActivity());
		getActivity().changeScene(1);
	}

	@Override
	public void managedUpdate(float arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sceneTouched(TouchEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void starting() 
	{
		super.createInputDialog(R.string.titleResourceId, R.string.messageResourceId, R.string.errorMessageId);
	}

}
