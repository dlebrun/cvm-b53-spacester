package com.example.spacester.scene;

import org.andengine.input.touch.TouchEvent;
import org.andengine.util.color.Color;

import com.example.spacester.sprite.RetourSprite;

import ca.qc.cvm.cvmandengine.entity.CVMText;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class CreditsScene extends CVMAbstractScene
{

	public CreditsScene() 
	{
		super("background/creditscreen/creditscene.png", 3);
		
		/* #####################################
		 * 			CreditScene - Text
		 * ##################################### */
		CVMText text = new CVMText(CVMGameActivity.CAMERA_WIDTH/2-350, CVMGameActivity.CAMERA_HEIGHT/2-150, 40, "Cr�dits", Color.WHITE);
		CVMText text2 = new CVMText(CVMGameActivity.CAMERA_WIDTH/2-350, CVMGameActivity.CAMERA_HEIGHT/2-90, 20, "Titre: Spacester", Color.WHITE);
		CVMText text3 = new CVMText(CVMGameActivity.CAMERA_WIDTH/2-350, CVMGameActivity.CAMERA_HEIGHT/2-50, 20, "D�veloppeur: David Lebrun", Color.WHITE);
		CVMText text4 = new CVMText(CVMGameActivity.CAMERA_WIDTH/2-350, CVMGameActivity.CAMERA_HEIGHT/2-10, 20, "Projet: B54 C�gep du Vieux Montr�al", Color.WHITE);
		CVMText text5 = new CVMText(CVMGameActivity.CAMERA_WIDTH/2-350, CVMGameActivity.CAMERA_HEIGHT/2+30, 20, "Professeur: Fr�d�ric Th�riault", Color.WHITE);
		CVMText text6 = new CVMText(CVMGameActivity.CAMERA_WIDTH/2-350, CVMGameActivity.CAMERA_HEIGHT/2+70, 20, "Cr�dits suppl�mentaires : ", Color.WHITE);
		CVMText text7 = new CVMText(CVMGameActivity.CAMERA_WIDTH/2-250, CVMGameActivity.CAMERA_HEIGHT/2+110, 20, "1 - Fond d'�cran du GameScene par Olivier Martel", Color.WHITE);
		CVMText text8 = new CVMText(CVMGameActivity.CAMERA_WIDTH/2-250, CVMGameActivity.CAMERA_HEIGHT/2+150, 20, "2 - http://opengameart.org/", Color.WHITE);
		super.addText(text);
		super.addText(text2);
		super.addText(text3);
		super.addText(text4);
		super.addText(text5);
		super.addText(text6);
		super.addText(text7);
		super.addText(text8);
		
		//le bouton retour
		RetourSprite retour = new RetourSprite();
		super.addSprite(retour);
	}
	@Override
	public void managedUpdate(float arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void sceneTouched(TouchEvent touchEvent) 
	{
	}
	@Override
	public void starting() {
		// TODO Auto-generated method stub
	}
}
