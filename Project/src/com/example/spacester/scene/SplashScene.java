package com.example.spacester.scene;

import org.andengine.input.touch.TouchEvent;


import com.example.spacester.sprite.CreditSprite;
import com.example.spacester.sprite.PlaySprite;
import com.example.spacester.sprite.QuitSprite;
import com.example.spacester.sprite.ScoreSprite;
import com.example.spacester.sprite.TitleSplash;

import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;

public class SplashScene extends CVMAbstractScene 
{
	public SplashScene()
	{
		//Le background du SplashScreen
		super("background/splashscreen/splashback.png", 1); 
		
		//Musique de fond du splashscene
		super.setMusicPath("music/space_splash.ogg", true); 
		
		//Ajout des titres
		TitleSplash title = new TitleSplash();
		PlaySprite playTitle = new PlaySprite();
		ScoreSprite scoreTitle = new ScoreSprite();
		CreditSprite creditTitle = new CreditSprite();
		QuitSprite quitTitle = new QuitSprite();
		super.addSprite(title);
		super.addSprite(playTitle);
		super.addSprite(scoreTitle);
		super.addSprite(creditTitle);
		super.addSprite(quitTitle);
	}

	@Override
	public void managedUpdate(float arg0) 
	{
		// TODO Auto-generated method stub
	}

	public void sceneTouched(TouchEvent touchEvent) 
	{
	}

	@Override
	public void starting() {
		// TODO Auto-generated method stub
	}
}
