package com.example.spacester.scene;

import java.util.Random;


import org.andengine.input.touch.TouchEvent;
import org.andengine.util.color.Color;

import com.example.spacester.SoundMng;
import com.example.spacester.sprite.BulletSprite;
import com.example.spacester.sprite.MidEnemySprite;

import com.example.spacester.sprite.ShipSprite;
import com.example.spacester.sprite.SmallEnemySprite;
import com.example.spacester.sprite.TouchBulletSprite;

import ca.qc.cvm.cvmandengine.entity.CVMText;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;

public class GameScene extends CVMAbstractScene
{
	//Les variables 
	Random rand = new Random();
	private int randPosY = 0;
	private int oldScores = 0;
	private int newScores = 0;
	private int oldLife = 0;
	private int newLife = 0;
	private float secondsElapsed = 0;
	private float secondsElapsedMid = 0;
	private ShipSprite ship = new ShipSprite(50,50);
	private TouchBulletSprite touchBulletSprite = new TouchBulletSprite();
	public static CVMText scoresText;

	public GameScene() 
	{
		super("background/gamescreen/spaceback.jpg", 2); // Photo : Olivier Martel background du gamescene
		//creation de notre vaisseau
		super.addSprite(ship);
		//L'area touch bullet pour tirer
		super.addSprite(touchBulletSprite);
	}
	@Override
	public void sceneTouched(TouchEvent touchEvent) 
	{
		if(touchEvent.getAction() == TouchEvent.ACTION_MOVE)
		{
			//On d�place seulement si on est sur la position du vaisseau
			if(ShipSprite.touche == true)
			{
				ship.setPosX(touchEvent.getX());
				ship.setPosY(touchEvent.getY());
				//On d�place le vaisseau
				ship.getSprite().setPosition(touchEvent.getX(), touchEvent.getY());
			}
		}
		//Permet de gerer le multitouch
		if(touchEvent.getAction() == TouchEvent.ACTION_DOWN)
		{
			if(touchEvent.getX() >= 500 && touchEvent.getX() <= 800 && touchEvent.getY() >= 250 && touchEvent.getY() <= 480)
			{
				SoundMng.getInstance().playSound(SoundMng.SHOOT);
				super.addSprite(new BulletSprite(this.getShip().getPosX(),this.getShip().getPosY()));
			}
		}
		
		//Si on relache le vaisseau on remet a false pour qu'il ne puisse pas se d�place n'importe ou que l'on touche
		if(touchEvent.getAction() == TouchEvent.ACTION_UP)
		{
			ShipSprite.touche = false;
		}
	}

	@Override
	public void managedUpdate(float secondsElapsed) 
	{
		/* Variables */
		randPosY = rand.nextInt(350)+50;
		this.secondsElapsed += secondsElapsed;
		this.secondsElapsedMid += secondsElapsed;
		
		//� chaque 2 secondes nouveau vaisseau small s'ajoute
		if (this.secondsElapsed > 2) 
		{
			super.addSprite(new SmallEnemySprite(800,randPosY));
			this.secondsElapsed = 0f;
		}
		//� chaque 5 secondes on ajoute le mid enemy
		if(this.secondsElapsedMid > 5)
		{
			super.addSprite(new MidEnemySprite(800,randPosY));
			this.secondsElapsedMid = 0f;
		}
		oldLife = newLife;
		newLife = ShipSprite.playerLife;
		oldScores = newScores;
		newScores = ShipSprite.getPlayerScores();
		//Si lancien scores est moindre, on reaffiche et on actualise le scores
		if(oldScores < newScores || oldLife > newLife)
		{
			//on supprime l,affichage
			super.removeText(scoresText);
			//On actualise
			scoresText = new CVMText(100, 20, 20, "Scores: "+ShipSprite.getPlayerScores()+"  Vies :"+ShipSprite.playerLife,Color.WHITE);
			//on re affiche 
			super.addText(scoresText);
		}
	}
	
	@Override
	public void starting() 
	{
		ShipSprite.setPlayerLife(3);
		ShipSprite.setPlayerScores(0);
		ShipSprite.setPlayerName("");scoresText = new CVMText(100, 20, 20, "Scores: "+ShipSprite.getPlayerScores()+"  Vies :"+ShipSprite.playerLife,Color.WHITE);
		super.addText(scoresText);
	}

	public ShipSprite getShip() {
		return ship;
	}

	public void setShip(ShipSprite ship) {
		this.ship = ship;
	}


	public static CVMText getScoresText() {
		return scoresText;
	}


	public static void setScoresText(CVMText scoresText) {
		GameScene.scoresText = scoresText;
		
	}
	
}
