package com.example.spacester.scene;

import java.util.List;

import org.andengine.input.touch.TouchEvent;
import org.andengine.util.color.Color;

import com.example.spacester.entity.Player;
import com.example.spacester.model.ModelPlayer;
import com.example.spacester.sprite.RetourSprite;

import ca.qc.cvm.cvmandengine.entity.CVMText;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class ScoreScene extends CVMAbstractScene
{
	int i = 0;
	CVMGameActivity activity = null;

	public ScoreScene(CVMGameActivity activity) 
	{
		super("background/creditscreen/creditscene.png", 4);
		this.activity = activity;
		//le bouton retour
		RetourSprite retour = new RetourSprite();
		super.addSprite(retour);
	}

	@Override
	public void managedUpdate(float arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void sceneTouched(TouchEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void starting() 
	{
		
		ModelPlayer modelPlayer = new ModelPlayer(activity);
		modelPlayer.openDatabase();
		List<Player> playerList = modelPlayer.fetchRooms();
		super.addText(new CVMText(350,5, 50, "Top 10", Color.WHITE));
    	for(Player player : playerList)
    	{
    		if(i+1 <= 10)
    		{
    			super.addText(new CVMText(250,70+(i*40), 25, i+1+"-"+" Nom: "+player.getName(), Color.WHITE));
    			super.addText(new CVMText(500,70+(i*40), 25, "Scores: "+player.getScores(), Color.WHITE));
    			i++;
    		}
    	}
    	//ferme la database
    	modelPlayer.closeDatabase();

	}
}
