package com.example.spacester;
import ca.qc.cvm.cvmandengine.CVMSoundManager;
import ca.qc.cvm.cvmandengine.entity.CVMSound;



public class SoundMng extends CVMSoundManager 
{
	public static final int EXPLOSION = 1;
	public static final int SHOOT = 2;

	private static SoundMng instance;
	
	private SoundMng() 
	{
		super.addSound(new CVMSound(EXPLOSION, "sound/explosion.flac"));
		super.addSound(new CVMSound(SHOOT, "sound/lasershot.wav"));
	}
	
	public static SoundMng getInstance() {
		if (instance == null) {
			instance = new SoundMng();
		}
		
		return instance;
	}
}

