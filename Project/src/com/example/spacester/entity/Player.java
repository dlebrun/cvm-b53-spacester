package com.example.spacester.entity;

public class Player 
{
	private int id;
	private String name;
	private int scores;
	
	public Player(int id, String name, int scores)
	{
		this.id = id;
		this.name = name;
		this.scores = scores;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getScores() {
		return scores;
	}

	public void setScores(int scores) {
		this.scores = scores;
	}

}
