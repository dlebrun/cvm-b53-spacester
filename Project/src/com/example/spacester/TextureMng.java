package com.example.spacester;
import ca.qc.cvm.cvmandengine.CVMTextureManager;
import ca.qc.cvm.cvmandengine.entity.CVMTexture;

public class TextureMng extends CVMTextureManager 
{
	/**
	 * LES SPRITES
	 */
	public static final int TITLE_SPLASH = 2;
	public static final int TITLE_PLAY = 3;
	public static final int TITLE_CREDITS = 4;
	public static final int TITLE_QUIT = 5;
	public static final int TITLE_RETOUR = 6;
	public static final int TITLE_SCORE = 7;
	public static final int SPRITE_TOUCH_BULLET = 8; 
	public static final int SPRITE_SHIP = 100;
	public static final int SPRITE_SHIP_SMALL_ENEMY = 101;
	public static final int SPRITE_SHIP_MID_ENEMY = 102;
	public static final int SPRITE_BULLET = 103;
	public static final int PARTICLE_POINT = 200;
	
	private static TextureMng instance;
	
	private TextureMng()
	{
		// cr�ation des titres 
		super.addTexture(new CVMTexture(TITLE_SPLASH,"titles/title_splash.png",585,184));
		super.addTexture(new CVMTexture(TITLE_PLAY,"titles/play.png",142,68));
		super.addTexture(new CVMTexture(TITLE_CREDITS,"titles/credits.png",142,68));
		super.addTexture(new CVMTexture(TITLE_QUIT,"titles/quit.png",142,68));
		super.addTexture(new CVMTexture(TITLE_RETOUR,"titles/retour.png",142,68));
		super.addTexture(new CVMTexture(TITLE_SCORE,"titles/score.png",142,68));
		
		//les vaisseaux alli�e et ennemie
		super.addTexture(new CVMTexture(SPRITE_SHIP, "logo/logo.png",128,128));
		super.addTexture(new CVMTexture(SPRITE_SHIP_SMALL_ENEMY,"sprites/small_enemy.png",168,55,4,1));
		super.addTexture(new CVMTexture(SPRITE_SHIP_MID_ENEMY,"sprites/mid_enemy.png",192,34,4,1));
		
		//les missiles (bullet)
		super.addTexture(new CVMTexture(SPRITE_BULLET,"sprites/bullet.png",19,24));
		
		//le boutton pour pouvoir tirer
		super.addTexture(new CVMTexture(SPRITE_TOUCH_BULLET, "sprites/cible.png",156,149));
		
		//les particles
		super.addTexture(new CVMTexture(PARTICLE_POINT,"particles/particle_point.png",32,32));
		
	}
	//le pattern singleton
	public static TextureMng getInstance()
	{
		if(instance == null)
		{
			instance = new TextureMng();
		}
		return instance;
	}
}
