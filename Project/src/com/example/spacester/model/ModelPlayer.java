package com.example.spacester.model;

import java.util.ArrayList;
import java.util.List;

import com.example.spacester.entity.Player;
import com.example.spacester.model.db.DatabaseHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ModelPlayer 
{
	private DatabaseHelper databaseHelper;
	private SQLiteDatabase database;

	public ModelPlayer(Context ctx) 
	{
		databaseHelper = new DatabaseHelper(ctx);
	}
	
	public void openDatabase() 
	{
		database = databaseHelper.getWritableDatabase();
	}
	public void closeDatabase() 
	{
		database.close();
	}
	public long addPlayer(Player player) 
	{
		ContentValues values = new ContentValues();
		values.put("nom", player.getName());
		values.put("scores", player.getScores());
		//le id on le passe pas parce qu'il a un autoincrement dessus
				
		return database.insert("players", null, values);
	}

	public List<Player> fetchRooms() 
	{
		List<Player> playerList = new ArrayList<Player>();
		String list [] = {"id","scores", "nom"};
		Cursor cursor = database.query(true, "players", list, null, null, null,null,list[1]+" DESC", null);
		
		if(cursor != null && cursor.moveToFirst())
		{
			do 
			{
				playerList.add(new Player(cursor.getInt(0), 
									  cursor.getString(2), 
									  cursor.getInt(1)));
			}
			while (cursor.moveToNext());//aller au prochain si null, quitter 
		}
		return playerList;
	}

}
