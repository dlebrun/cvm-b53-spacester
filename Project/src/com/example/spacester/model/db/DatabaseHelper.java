package com.example.spacester.model.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper 
{
	private static final String DATBASE_NAME = "db_player";
	private static final int DATABASE_VERSION = 1;
	
	public DatabaseHelper(Context context) 
	{
		super(context, DATBASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) 
	{
		try 
		{
			//On creer la table players
			db.execSQL("CREATE TABLE players (" + 
						"  id 		INTEGER PRIMARY KEY AUTOINCREMENT," +
						"  scores 	INTEGER,      " +
						"  nom 		TEXT  " + 
						")");
			Log.i("CVMSQLite", "BD cr�er");
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
	{
		db.execSQL("DROP TABLE IF EXISTS players");
		onCreate(db);
	}
}

