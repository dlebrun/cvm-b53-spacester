package com.example.spacester.sprite;

import org.andengine.input.touch.TouchEvent;

import com.example.spacester.TextureMng;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.TouchAreaListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class CreditSprite extends CVMSprite implements TouchAreaListener
{
	public CreditSprite() 
	{
		super(350,325,142,68, TextureMng.TITLE_CREDITS);
	}

	@Override
	public void onAreaTouched(TouchEvent touchEvent, float localX, float localY, CVMGameActivity activity, CVMAbstractScene scene) 
	{
		//Change de scene a la scene de credit
		if(touchEvent.getAction() == TouchEvent.ACTION_UP)
		{
			activity.changeScene(3, true);
		}
	}

}
