package com.example.spacester.sprite;

import org.andengine.input.touch.TouchEvent;

import com.example.spacester.TextureMng;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.TouchAreaListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class QuitSprite extends CVMSprite implements TouchAreaListener
{
	public QuitSprite() 
	{
		super(350,400,142,68, TextureMng.TITLE_QUIT);
	}

	@Override
	public void onAreaTouched(TouchEvent touchEvent, float localX, float localY, CVMGameActivity activity, CVMAbstractScene scene) 
	{
		//quitte le jeux
		if(touchEvent.getAction() == TouchEvent.ACTION_UP)
		{
			activity.finish();
		}
	}

}
