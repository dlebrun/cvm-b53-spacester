package com.example.spacester.sprite;
import com.example.spacester.TextureMng;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;

public class TitleSplash extends CVMSprite
{
	public TitleSplash()
	{
		super(100, 20,545,184, TextureMng.TITLE_SPLASH);
	}
}
