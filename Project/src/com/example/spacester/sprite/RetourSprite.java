package com.example.spacester.sprite;

import org.andengine.input.touch.TouchEvent;

import com.example.spacester.TextureMng;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.TouchAreaListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class RetourSprite extends CVMSprite implements TouchAreaListener
{
	public RetourSprite() 
	{
		super(0,0,142,68, TextureMng.TITLE_RETOUR);
	}
	@Override
	public void onAreaTouched(TouchEvent touchEvent, float localX, float localY, CVMGameActivity activity, CVMAbstractScene scene) 
	{
		//Change de scene au menu
		if(touchEvent.getAction() == TouchEvent.ACTION_UP)
		{
			activity.changeScene(1);
		}
		
	}

}
