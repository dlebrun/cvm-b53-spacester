package com.example.spacester.sprite;

import org.andengine.input.touch.TouchEvent;

import android.util.Log;

import com.example.spacester.SoundMng;
import com.example.spacester.TextureMng;
import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.CollisionListener;
import ca.qc.cvm.cvmandengine.entity.ManagedUpdateListener;
import ca.qc.cvm.cvmandengine.entity.TouchAreaListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class BulletSprite extends CVMSprite implements TouchAreaListener, ManagedUpdateListener, CollisionListener
{
	//Variables
	private float secondsElapsed = 0;
	private float speed = 200;
	private float x = 0;
	

	public BulletSprite(float posX, float posY) 	//On va donner la position en param�tre pour dire au bullet ou se placer
	{
		super(posX,posY,25,25,TextureMng.SPRITE_BULLET);
	}

	@Override
	public void collidedWith(CVMGameActivity activity, CVMAbstractScene scene,CVMSprite collisionSprite) 
	{
		if(collisionSprite instanceof SmallEnemySprite)
		{
			SoundMng.getInstance().playSound(SoundMng.EXPLOSION);
			((SmallEnemySprite)collisionSprite).explode(scene);
			scene.removeSprite(this);
			ShipSprite.playerScores+=25;
		}
		if(collisionSprite instanceof MidEnemySprite)
		{
			SoundMng.getInstance().playSound(SoundMng.EXPLOSION);
			((MidEnemySprite)collisionSprite).explode(scene);
			scene.removeSprite(this);
			ShipSprite.playerScores+=50;
		}
	}

	@Override
	public void managedUpdate(float secondsElapsed, CVMGameActivity activity,CVMAbstractScene scene) 
	{
		this.secondsElapsed += secondsElapsed;
		//apres 2.5 secondes on supprimes le missiles
		if(this.secondsElapsed < 2.5)
		{
			x += secondsElapsed * speed;
			this.getSprite().setPosition(this.getInitialX()+x, this.getInitialY()+25);
		}
		else
		{
			scene.removeSprite(this);
			this.secondsElapsed = 0f;
		}
	}

	@Override
	public void onAreaTouched(TouchEvent arg0, float arg1, float arg2,
			CVMGameActivity arg3, CVMAbstractScene arg4) {
		// TODO Auto-generated method stub
		
	}
}
