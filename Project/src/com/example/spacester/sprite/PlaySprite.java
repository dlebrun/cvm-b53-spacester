package com.example.spacester.sprite;

import org.andengine.input.touch.TouchEvent;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.TouchAreaListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

import com.example.spacester.TextureMng;

public class PlaySprite extends CVMSprite implements TouchAreaListener
{
	public PlaySprite()
	{
		super(350, 180,142,68, TextureMng.TITLE_PLAY);
	}

	@Override
	public void onAreaTouched(TouchEvent touchEvent, float localX, float localY,
			CVMGameActivity activity, CVMAbstractScene scene) 
	{
		//Change de scene � la scene de jeux
		if(touchEvent.getAction() == TouchEvent.ACTION_UP)
		{
			//On change de scene a la GameScene
			activity.changeScene(2);
		}
		
	}

}
