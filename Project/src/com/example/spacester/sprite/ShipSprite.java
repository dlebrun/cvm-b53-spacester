package com.example.spacester.sprite;

import org.andengine.input.touch.TouchEvent;

import com.example.spacester.MainActivity;
import com.example.spacester.TextureMng;
import com.example.spacester.scene.GameScene;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.ManagedUpdateListener;
import ca.qc.cvm.cvmandengine.entity.TouchAreaListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class ShipSprite extends CVMSprite implements ManagedUpdateListener, TouchAreaListener
{
	/* Variables */
	public static String playerName = "";
	public static int playerScores = 0;	//Si on �limine un small enemy on gagne 25 points, si on �limine un mid on gagne 50 points
	public static int playerLife = 3;		//Au d�part on n'a 3 vies, si on laisse pass� un enemy on perd une vie
	public static boolean touche = false;
	public static float posX = 0;
	public static float posY = 0;	
	
	public ShipSprite(float posX, float posY)
	{
		super(posX,posY,80,80,TextureMng.SPRITE_SHIP);
		this.posX=posX;
		this.posY =posY;
	}

	@Override
	public void managedUpdate(float secondElapsed, CVMGameActivity activity, CVMAbstractScene scene) 
	{
		//Si la vie est plus petite ou �gale a 0 on enregistre le score et le nom, ensuite retour au menu
		if(playerLife <= 0)
		{
			activity.changeScene(5);
		}		
	}

	@Override
	public void onAreaTouched(TouchEvent touchEvent, float localX, float localY, CVMGameActivity activity, CVMAbstractScene scene) 
	{
		//Seulement Si on clique sur le sprite on peut le d�placer
		if(touchEvent.getAction() == TouchEvent.ACTION_DOWN)
		{
			touche = true;
		}
	}

	public static float getPosX() {
		return posX;
	}

	public static void setPosX(float posX) {
		ShipSprite.posX = posX;
	}

	public static float getPosY() {
		return posY;
	}

	public static void setPosY(float posY) {
		ShipSprite.posY = posY;
	}

	public static int getPlayerScores() {
		return playerScores;
	}

	public static void setPlayerScores(int playerScores) {
		ShipSprite.playerScores = playerScores;
	}

	public static String getPlayerName() {
		return playerName;
	}

	public static void setPlayerName(String playerName) {
		ShipSprite.playerName = playerName;
	}

	public static int getPlayerLife() {
		return playerLife;
	}

	public static void setPlayerLife(int playerLife) {
		ShipSprite.playerLife = playerLife;
	}
}
