package com.example.spacester.sprite;

import org.andengine.entity.sprite.AnimatedSprite;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.CollisionListener;
import ca.qc.cvm.cvmandengine.entity.ManagedUpdateListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

import com.example.spacester.TextureMng;

public class MidEnemySprite extends CVMSprite implements ManagedUpdateListener
{
	private float secondsElapsed = 0;
	private float speed = 75;
	private float x = 0;
	public float posX = 0;
	public float posY = 0;
	private boolean started = false;

	public MidEnemySprite(float posX, float posY) 
	{
		super(posX, posY,59 ,44 , TextureMng.SPRITE_SHIP_MID_ENEMY);
		this.posX = posX;
		this.posY = posY;
	}

	public void managedUpdate(float secondsElapsed, CVMGameActivity activity, CVMAbstractScene scene) 
	{
		if(!started)
		{
			started = true;
			AnimatedSprite animatedSprite = (AnimatedSprite)this.getSprite();
			animatedSprite.animate(175, true);
		}
		this.secondsElapsed += secondsElapsed;
		x += secondsElapsed * speed;
		this.getSprite().setPosition(this.getInitialX()-x, this.getInitialY());
		this.posX = this.getInitialX()-x;
		this.posY = this.getInitialY();
		
		if(this.getSprite().getX() <= 0)
		{
			scene.removeSprite(this);
			ShipSprite.playerLife--;
		}
	}
	public float getPosX() {
		return posX;
	}

	public void setPosX(float posX) {
		this.posX = posX;
	}

	public float getPosY() {
		return posY;
	}

	public void setPosY(float posY) {
		this.posY = posY;
	}

	public void explode(CVMAbstractScene scene) 
	{
		scene.removeSprite(this);
		ParticlesFactory.addExplosion(x, this.getInitialY(), scene);
	}
}

