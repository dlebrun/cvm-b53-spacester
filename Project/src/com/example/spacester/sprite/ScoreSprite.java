package com.example.spacester.sprite;

import org.andengine.input.touch.TouchEvent;

import com.example.spacester.TextureMng;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.TouchAreaListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class ScoreSprite extends CVMSprite implements TouchAreaListener
{
	public ScoreSprite() 
	{
		super(350,255,142,68, TextureMng.TITLE_SCORE);
	}

	@Override
	public void onAreaTouched(TouchEvent touchEvent, float localX, float localY,
			CVMGameActivity activity, CVMAbstractScene scene) 
	{
		if(touchEvent.getAction() == TouchEvent.ACTION_UP)
		{
			activity.changeScene(4);
		}
	}

}
