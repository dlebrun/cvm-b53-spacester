package com.example.spacester;

import java.util.ArrayList;
import java.util.List;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

import com.example.spacester.entity.Player;
import com.example.spacester.model.ModelPlayer;
import com.example.spacester.scene.CreditsScene;
import com.example.spacester.scene.GameOverScene;
import com.example.spacester.scene.GameScene;
import com.example.spacester.scene.ScoreScene;
import com.example.spacester.scene.SplashScene;

public class MainActivity extends CVMGameActivity 
{
    public MainActivity() 
    {
    	super(TextureMng.getInstance());
    	super.setSoundManager(SoundMng.getInstance());
    	// Cr�ation des sc�nes et ajout de ces sc�nes � l'activit�
		List<CVMAbstractScene> sceneList = new ArrayList<CVMAbstractScene>();
		sceneList.add(new SplashScene());
		sceneList.add(new GameScene());
		sceneList.add(new CreditsScene());
		sceneList.add(new ScoreScene(MainActivity.this));
		sceneList.add(new GameOverScene());
		super.setSceneList(sceneList); 
	}
    
   /**
    * Permet d'ajouter les joueurs � la base de donn�es
    * @param name
    * @param scores
    * @param activity
    */
    public static void databasePlayer(String name, int scores, CVMGameActivity activity) 
    {
    	ModelPlayer modelPlayer = new ModelPlayer(activity);
    	//ouvre la database
    	modelPlayer.openDatabase();
    	//Ajouter un joueurs
    	modelPlayer.addPlayer(new Player(0,name, scores));
    	//ferme la database
    	modelPlayer.closeDatabase();
    }
}
